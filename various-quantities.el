;;; various-quantities.el --- Command to select actions to perform on various quantities -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Edgard Bikelis

;; Author: Edgard Bikelis <bikelis@gmail.com>
;; URL: https://edgard.bikelis.com/various-quantities
;; Package-Requires: ((emacs "26.1"))
;; Keywords: lisp
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Various Quantities implements an alternative way of dealing with
;; ‘forward-’ing, ‘backward-’ing ‘kill-’ing etc various quantities
;; like a ‘char’, a ‘paragraph’, a ‘line’ and so forth, by asking
;; interactively what action to be performed on what quantity, making
;; it arguably more intuitive, and simultaneously showing the user the
;; name of the function being used, and to which key combination it is
;; bound.  This might help users remembering the simple commands.  This
;; package is inspired by a similar command on Lisp Machines, which
;; was bound to C-q along with the equivalent of ‘quote-insert’, in
;; such a way that if the char following that key sequence wasn’t an
;; “exotic” character like , the ‘various-quantities’ command was
;; invoked instead.  For this, some new functions had to be made, in
;; order to fulfill all possibilities, like ‘delete-word’, which
;; doesn’t exist.

;;; Code:

(defun various-quantities--control-character-p (char)
  "I check if the given CHAR is a control character."
  (eq (get-char-code-property char 'general-category) 'Cc))

(defun various-quantities--generate-menu-string (name lst &optional separator-cons)
  "I create a string for the menu named NAME from the given LST.

LST must have lists as members, in the following format:
\(character name-of-option symbol-of-option).  The symbol is going
to be returned.

SEPARATOR-CONS might be used to envelop the character of each
option, like [c]."
  (unless separator-cons
    (setq separator-cons '("" . "")))
  (unless (equal name "")
    (setq name (concat name ":")))
  (let ((options nil)
	(max-width 0)
	(width-limit (if (> (frame-width) 75)
			 80
		       (if (< (frame-width) 40)
			   50
			 (frame-width))))
	(menu name))
    (dolist (item lst)
      (let ((this-output (concat
			  (if (various-quantities--control-character-p (nth 0 item)) "" " ")
			  (propertize
			   (concat (car separator-cons)
				   (char-to-string (nth 0 item))
				   (cdr separator-cons))
			   'face 'help-key-binding)
			  " "
			  (nth 1 item))))
	(push this-output options)
	(unless (< (length this-output) max-width)
	  (setq max-width (length this-output)))))
    (let ((line-width 0))
      (dolist (option (nreverse options))
	;; add padding
	(setq option (format (concat "%-" (int-to-string (1+ max-width)) "s")
			     option))
	;; if adding the next item would overflow the line
	(if (>= (+ line-width max-width (length option)) width-limit)
	    ;; we add a newline, add the NAME so the options get
	    ;; aligned, reset the line-width, and concat the item
	    (progn (setq menu (concat menu
				      (char-to-string ?\C-j)
				      (format (concat "%-" (int-to-string (length name)) "s") "")))
		   (setq menu (concat menu option))
		   (setq line-width (+ (length name) (length option))))
	  ;; otherwise we add the option and increment its length to
	  ;; the line-width
	  (progn (setq menu (concat menu option))
		 (setq line-width (+ line-width (length option)))))))
    menu))

(defvar various-quantities-action-table
      '((?f "forward"   forward)
	(?b "backward"  backward)
	(?d "delete"    delete) ;; kills forward
	(?r "rubout"    rubout) ;; kills backward
	(?c "copy "     copy)
	(?k "kill"      kill)
	(?t "transpose" transpose) ;; ‘twiddle’ in olden times
	(?@ "mark"      mark)
	(?u "uppercase" upcase)
	(?l "lowercase" downcase))
      "Alist used by ‘various-quantities--generate-menu-string’.")

(defvar various-quantities-quantity-table
      '((?c    "character" char)
	(?w    "word"      word)
	(?s    "sentence"  sentence)
	(?p    "paragraph" paragraph)
	(?l    "line"      line)
	(?-    "s-exp"     sexp)
	(?\(   "list"      list)
	(?d    "defun"     defun)
	(?\C-l "page"      page) ;;  since ?l is already taken
	(?b    "buffer"    buffer)
	(??    "help"      help))
      "Alist used by used by ‘various-quantities--generate-menu-string’.")



;;
;; CUSTOM COMMANDS
;;

;; FORWARD

(defun various-quantities-forward-defun (&optional prefix)
  "I move the point to the end of PREFIX number of DEFUNs."
  (unless prefix
    (setq prefix 1))
  (end-of-defun (+ prefix 1))
  (beginning-of-defun))



;; BACKWARD

(defun various-quantities-backward-defun (&optional prefix)
  "I move the point to the beginning of PREFIX number of DEFUNs."
  (unless prefix
    (setq prefix 1))
  (beginning-of-defun prefix))



;; MARK

(defun various-quantities-mark-char (&optional prefix)
  "I mark the PREFIX number of characters."
  (set-mark (point))
  (forward-char prefix))

(defun various-quantities-mark-sentence (&optional prefix)
  "I mark the PREFIX number of sentences."
  (set-mark (point))
  (forward-sentence prefix))

(defun various-quantities-mark-line (&optional prefix)
  "I mark the PREFIX number of lines."
  (set-mark (point))
  (forward-line prefix))

(defun various-quantities-mark-buffer (&optional prefix)
  "I mark the whole buffer.  PREFIX is ignored."
  (ignore prefix)
  (set-mark (point-min))
  (goto-char (point-max)))



;; DELETE

(defun various-quantities-delete-word (&optional prefix)
  "I delete, without killing, the PREFIX number of words."
  (mark-word prefix)
  (delete-region (point) (mark))
  (just-one-space))

(defun various-quantities-delete-sentence (&optional prefix)
  "I delete, without killing, the PREFIX number of sentences."
  (various-quantities-mark-sentence prefix)
  (delete-region (point) (mark))
  (just-one-space))

(defun various-quantities-delete-paragraph (&optional prefix)
  "I delete, without killing, the PREFIX number of paragraphs."
  (mark-paragraph prefix)
  (delete-region (point) (mark)))

(defun various-quantities-delete-sexp (&optional prefix)
  "I delete, without killing, the PREFIX number of sexps."
  (mark-sexp prefix)
  (delete-region (point) (mark)))

(defun various-quantities-delete-page (&optional prefix)
  "I delete, without killing, the PREFIX number of pages."
  (mark-page prefix)
  (delete-region (point) (mark)))



;; RUBOUT

(defun various-quantities-rubout-char (&optional prefix)
  "I delete backwards, without killing, the PREFIX number of characters."
  (unless prefix
    (setq prefix 1))
  (delete-char (- prefix)))

(defun various-quantities-rubout-word (&optional prefix)
  "I delete backwards, without killing, the PREFIX number of words."
  (unless prefix
    (setq prefix 1))
  (mark-word (- prefix))
  (delete-region (point) (mark)))

(defun various-quantities-rubout-sentence (&optional prefix)
  "I delete backwards, without killing, the PREFIX number of sentences."
  (unless prefix
    (setq prefix 1))
  (various-quantities-mark-sentence (- prefix))
  (delete-region (point) (mark)))

(defun various-quantities-rubout-paragraph (&optional prefix)
  "I delete backwards, without killing, the PREFIX number of paragraphs."
  (unless prefix
    (setq prefix 1))
  (mark-paragraph (- prefix))
  (delete-region (point) (mark)))

(defun various-quantities-rubout-line (&optional prefix)
  "I delete backwards, without killing, the PREFIX number of lines."
  (unless prefix
    (setq prefix 1))
  (various-quantities-mark-line (- prefix))
  (delete-region (point) (mark)))



;; KILL

(defun various-quantities-kill-char (&optional prefix)
  "I kill the PREFIX number of characters."
  (various-quantities-mark-char prefix)
  (kill-region (point) (mark)))

(defun various-quantities-kill-page (&optional prefix)
  "I kill the PREFIX number of pages."
  (mark-page prefix)
  (kill-region (point) (mark)))

(defun various-quantities-kill-buffer ()
  "I kill the whole buffer."
  (kill-region (point-min) (point-max)))



;; UPCASE

(defun various-quantities-upcase-sentence (&optional prefix)
  "I put the PREFIX number of sentences in upper case."
  (various-quantities-mark-sentence prefix)
  (upcase-region (point) (mark)))

(defun various-quantities-upcase-paragraph (&optional prefix)
  "I put the PREFIX number of paragraphs in upper case."
  (mark-paragraph prefix)
  (upcase-region (point) (mark)))

(defun various-quantities-upcase-line (&optional prefix)
  "I put the PREFIX number of lines in upper case."
  (various-quantities-mark-line prefix)
  (upcase-region (point) (mark)))

(defun various-quantities-upcase-sexp (&optional prefix)
  "I put the PREFIX number of sexps in upper case."
  (mark-sexp prefix)
  (upcase-region (point) (mark)))

(defun various-quantities-upcase-page (&optional prefix)
  "I put the PREFIX number of pages in upper case."
  (mark-page prefix)
  (upcase-region (point) (mark)))

(defun various-quantities-upcase-buffer (&optional prefix)
  "I put the whole of the current buffer in upper case.  PREFIX is ignored."
  (ignore prefix)
  (save-excursion
    (various-quantities-mark-buffer)
  (upcase-region (point) (mark))))



;; DOWNCASE

(defun various-quantities-downcase-char (&optional prefix)
  "I put the PREFIX number of characters in lower case."
  (various-quantities-mark-char prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-sentence (&optional prefix)
  "I put the PREFIX number of sentences in lower case."
  (various-quantities-mark-sentence prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-paragraph (&optional prefix)
    "I put the PREFIX number of paragraphs in lower case."
  (mark-paragraph prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-line (&optional prefix)
  "I put the PREFIX number of lines in lower case."
  (various-quantities-mark-line prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-sexp (&optional prefix)
  "I put the PREFIX number of sexps in lower case."
  (mark-sexp prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-page (&optional prefix)
  "I put the PREFIX number of pages in lower case."
  (mark-page prefix)
  (downcase-region (point) (mark)))

(defun various-quantities-downcase-buffer (&optional prefix)
  "I put the whole of the current buffer in lower case.  PREFIX is ignored."
  (ignore prefix)
  (save-excursion
    (various-quantities-mark-buffer)
    (downcase-region (point) (mark))))



;; COPY

(defun various-quantities-copy-char (&optional prefix)
  "I copy, without killing, a character PREFIX times just after it."
  (various-quantities-mark-char)
  (unless prefix (setq prefix 1))
  (let ((thing (buffer-substring (point) (mark))))
    (cl-loop repeat prefix do
	     (insert thing))))

(defun various-quantities-copy-word (&optional prefix)
  "I copy, without killing, a word PREFIX times just after it."
  (when current-prefix-arg
    (setq prefix current-prefix-arg))
  (unless prefix (setq prefix 1))
  (let ((thing (thing-at-point 'word)))
    (forward-word)
    (cl-loop repeat prefix do
		 (insert (concat " " thing)))))

(defun various-quantities-copy-sentence (&optional prefix)
  "I copy, without killing, a sentence PREFIX times just after it."
  (unless prefix (setq prefix 1))
  (let ((thing (thing-at-point 'sentence)))
    (forward-sentence)
    (cl-loop repeat prefix do
	     (insert thing))))

(defun various-quantities-copy-paragraph (&optional prefix)
  "I copy, without killing, a paragraph PREFIX times just after it."
  (unless prefix (setq prefix 1))
  (let ((thing (thing-at-point 'paragraph)))
    (forward-paragraph)
    (cl-loop repeat prefix do
	     (insert thing))))

(defun various-quantities-copy-line (&optional prefix)
  "I copy, without killing, a line PREFIX times just after it."
  (unless prefix (setq prefix 1))
  (let ((thing (thing-at-point 'line)))
    (forward-line)
    (cl-loop repeat prefix do
	     (insert thing))))

(defun various-quantities-copy-sexp (&optional prefix)
  "I copy, without killing, a sexp PREFIX times just after it."
  (unless prefix (setq prefix 1))
  (let (thing)
    (setq thing (thing-at-point 'sexp t))
    ;; if we are at the end of a line
    (if (equal (thing-at-point 'char t) (char-to-string ?\C-j))
	(cl-loop repeat prefix do
		 (insert (concat (char-to-string ?\C-j)
				 (char-to-string ?\C-j)
				 thing)))
      (progn (forward-sexp)
	     (cl-loop repeat prefix do
		      (insert (concat " " thing)))))))



;; command machinery

(defvar various-quantities-command-table
  '(((forward line)        next-line)
    ((backward line)       previous-line)
    ((kill buffer)         various-quantities-kill-buffer)
    ((delete buffer)       erase-buffer))
  "This list helps the ‘various-quantities-function-guesser’ deciding what to do.")

(defvar various-quantities-help-messages
  '((forward   "Moves forward according to the given quantity.")
    (backward  "Moves backward according to the given quantity.")
    (copy      "Copies forward the given quantity, without touching the kill-ring.")
    (delete    "Deletes forward the given quantity, without touching the kill-ring.")
    (kill      "Kills forward according to the given quantity.")
    (rubout    "Kills backward according to the given quantity.")
    (transpose "Exchanges things according to the given quantity.")
    (mark      "Marks according to the given quantity.")
    (save      "Saves on the kill-ring according to the given quantity.")
    (upcase    "Uppercase according to the given quantity.")
    (downcase  "Lowercase according to the given quantity.")))

(defun various-quantities-help (action)
  "I build the help message function for the ACTION given."
  (lambda ()
    (message (concat (symbol-name action)
		     ": "
		     (cadr (assoc action various-quantities-help-messages))))))

(defun various-quantities-query-menu (name table &optional arg)
  "I get the answer from the list TABLE named NAME for ‘various-quantities’ to use.

ARG is expected to be an inherited PREFIX for the command."
  (let (choice answer)
    (setq choice (read-char (various-quantities--generate-menu-string name table)))
    (setq answer (nth 2 (assoc choice table)))
    (list :answer answer :arg arg :char choice)))

(defun various-quantities-function-guesser (action quantity)
  "I find the function to perform the desired ACTION on the given QUANTITY."
  (let (guess-name)
    ;; this tries to find the default function
    (setq guess-name (concat (symbol-name action)
			"-"
			(symbol-name quantity)
			;; ‘transpose’ uses the plural
			(when (eq action 'transpose) "s")))
    ;; if it fails, try using a function that we defined
    (unless (functionp (read guess-name))
      (setq guess-name (concat "various-quantities-" guess-name)))
    ;; returns the function, if we got one
    (when (functionp (read guess-name))
	(read guess-name))))

(defun various-quantities-dispatcher (action quantity prefix)
  "I find and call the function for the given ACTION and QUANTITY PREFIX times.

I receive the results from the sequence ‘various-quantities-action-menu’ ->
‘various-quantities-action-menu’, and the PREFIX argument of the
first function, and find the appropriate function to be called to
perform the given ACTION on the given QUANTITY."
  (let (command command-name where-is where-is-msg args)
    ;; we treat the help command first
    (when (eq quantity 'help)
      (setq command (various-quantities-help action)))
    ;; we search for an entry in our command-table
    (unless command
      (setq command (cadr (assoc (list action quantity) various-quantities-command-table))))
    ;; if it failed, we try guessing the function from what the user gave us
    (unless command
      (setq command (various-quantities-function-guesser action quantity)))
    ;; if it fails, we failed
    (unless command
      (error "Unknown command for ‘%s’ ‘%s’" action quantity))
    ;; we try checking whether the function is bound, and to what key sequence
    (when (symbolp command)
      (setq where-is (where-is-internal command))
      (setq command-name (symbol-name command)))
    ;; lambda commands don’t live inside symbols, so we have to baptize them
    (unless command-name
      (setq command-name "lambda"))
    (when where-is
      (setq where-is-msg (cl-loop for item in where-is
				  collect (concat (propertize (key-description item) 'face 'help-key-binding) ", ")))
      (setq where-is-msg where-is-msg)
      (setq where-is-msg (cl-subseq (apply 'concat where-is-msg) 0 -2)))
    (unless where-is-msg
      (setq where-is-msg "unbound"))
    (setq args (car (func-arity command)))
    ;; if the arity of the function is > 0 and we don’t have a prefix,
    ;; we have a problem. so we supply 1 as the default argument.
    (when (and (not prefix)
	       (>= args 1))
      (setq prefix (list 1)))
    (unless (listp prefix)
      (setq prefix (list prefix)))
    ;; here we actually do what we want
    (apply command prefix)
    (unless (eq quantity 'help)
      (message (concat command-name " (" where-is-msg ")")))))

(defun various-quantities (&optional action-char)
  "I start a menu sequence to take an ACTION-CHAR on some QUANTITY in the buffer.

I then dispatch the choice for the ‘various-quantities-dispatcher’
 function, who might find and execute the appropriate command.

If ACTION is sent, it means we get straight to the second menu.  This exists
so we can work with the command ‘control-q-with-various-quantities’."
  (interactive)
  (let ((prefix current-prefix-arg)
	action
	quantity)
    (if action-char
	(setq action (list :answer (nth 2 (assoc action-char various-quantities-action-table))
			   :arg prefix
			   :char action-char))
      (setq action (various-quantities-query-menu
		    "Various quantities"
		    various-quantities-action-table)))
    (unless (assoc (cl-getf action :char) various-quantities-action-table)
      (error "Various quantities: Character ‘%c’ has no action associated with it."
	     (cl-getf action :char)))
    (when (cl-getf action :answer)
      (setq quantity (various-quantities-query-menu
		      (symbol-name (cl-getf action :answer))
		      various-quantities-quantity-table)))
    (unless (assoc (cl-getf quantity :char) various-quantities-quantity-table)
      (error "Various quantities: Character ‘%c’ has no quantity associated with it."
	     (cl-getf quantity :char)))
    (when quantity
      (various-quantities-dispatcher
       (cl-getf action :answer)
       (cl-getf quantity :answer)
       prefix))))

(defun various-quantities-with-quoted-insert (&optional with-help)
  "I combine the behaviour of ‘quoted-insert’ and ‘various-quantities’.

If a control character is given, it is inserted as usual;
otherwise, the command is sent as an ACTION to ‘various-commands’.

WITH-HELP shows the command along with a help message."
  (interactive)
  (let* ((message
	  (if with-help
	      (concat "Enter a control char to be inserted, like "
		      (propertize "C-j" 'face 'help-key-binding)
		      ", or choose one action to apply"
		      (char-to-string ?\C-j)
		      "to various quantities:"
		      (char-to-string ?\C-j)
		      (various-quantities--generate-menu-string "" various-quantities-action-table))
	    (concat (substitute-command-keys "\\[various-quantities-with-quoted-insert]")
		    ": Enter a control char, an action for ‘various-quantities’, or "
		    (propertize "?" 'face 'help-key-binding)
		    " for help.")))
	 (char (read-char message)))
    ;; if it be a control character
    (if (eq (get-char-code-property char 'general-category) 'Cc)
	;; how to use the ‘quoted-insert’ machinery here, without
	;; copying the code?
	(insert (char-to-string char))
      (if (eq char ??)
	  (various-quantities-with-quoted-insert t)
	(various-quantities char)))
    nil))

(provide 'various-quantities)
;;; various-quantities.el ends here
