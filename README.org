* various-quantities

Various Quantities is an Emacs package that implements an alternative
way of dealing with ~forward~-ing, ~backward~-ing, ~kill~-ing
etc. /various quantities/ like a ~char~, a ~paragraph~, a ~line~ and
so forth, showing the bound key sequence for the command, if there be
one.


* command completion

|           | char | word | sentence | paragraph | line | sexp | page | buffer | help |
|-----------+------+------+----------+-----------+------+------+------+--------+------|
| forward   | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| backward  | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| delete    | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| kill      | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| transpose | ok   | ok   | ok       | ok        | ok   | ok   |      |        |      |
| mark      | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| upcase    | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| downcase  | ok   | ok   | ok       | ok        | ok   | ok   | ok   |        |      |
| copy      | ok   | ok   | ok       | ok        | ok   | TODO |      |        |      |
| count     |      |      |          |           |      |      |      |        |      |
| help      |      |      |          |           |      |      |      |        |      |


* screenshots

Using ~various-quantities-with-quoted-insert~.

** first menu

[[file:pasted/acb82a5098a260a2c96fd5fca9f2514f.png]]

** second menu

[[file:pasted/541b71fa79f59af6e2ba7ba44484a10f.png]]

** final message

[[file:pasted/6507257ebd07fd53d5fa608f7291d08e.png]]
